import {
  Container,
  Product,
  Button,
  Title,
  Search,
  Category,
  Text,
  Flex,
  Page,
} from "../components";
import { Button as ButtonAntd, message } from "antd";
import { useRouter } from "next/router";
import DB_Services from "../firebase/services";
import { useAuth } from "../context";

import { app, db } from "../firebase/clientApp";

import { collection, getDoc, query, where } from "firebase/firestore";
import { useCollection } from "react-firebase-hooks/firestore";
import SkeletonCustom from "../components/Skeleton";

export default function Basket() {
  const router = useRouter();
  const { authUser, loading } = useAuth();

  if (loading) return <p>Loading...</p>;

  if (authUser) {
    const Bill = ({ total }) => {
      return (
        <p
          style={{
            color: "rgb(229, 224, 85)",
            lineHeight: 2.5,
            fontFamily: "Metropolis-Regular",
          }}
        >
          {total}
        </p>
      );
    };
    const [basket, basketLoading, basketError] = useCollection(
      query(collection(db, "orders"), where("user", "==", authUser.uid)),
      {}
    );

    const getTotal = (arr) => {
      let tot = 0;
      arr.forEach((order) => {
        tot += order.data().product.price * order.data().quantity;
      });
      return tot;
    };

    const increment = (id) => {
      DB_Services.incrementOrder({ id })
        .then(() => console.log("incremented"))
        .catch((err) => console.log(err));
    };

    const decrement = (id) => {
      if (
        basket.docs.filter((order) => order.id == id)[0].data().quantity == 1
      ) {
        deleteOrder(id);
      }
      DB_Services.decrementOrder({ id })
        .then(() => console.log("decremented"))
        .catch((err) => console.log(err));
    };

    const deleteOrder = (id) => {
      DB_Services.deleteOrder({ id })
        .then(() => message.success("Item removed from cart"))
        .catch((err) => console.log(err));
    };

    const lipaNaMpesa = async () => {

    };

    return (
      <Page
        title="Basket"
        extra={<Bill total={`Ksh. ${basket ? getTotal(basket.docs) : 0}`} />}
      >

        <Container style={{ position: "absolute", top: 56, minWidth: "95vw" }}>
          {basketError && <p>Error...</p>}
          {basketLoading && (
            <Flex scrollY style={{ paddingBottom: "5rem" }}>
              {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((el) => (
                <SkeletonCustom />
              ))}
            </Flex>
          )}

          {basket && basket.docs.length > 0 && (
            <Flex scrollY style={{ paddingBottom: "5rem" }}>
              {basket.docs.map((order) => (
                <Product
                  name={order.data().product.name}
                  price={order.data().product.price}
                  quantity={order.data().quantity}
                  rounded
                  image={order.data().product.image}
                  basket
                  id={order.id}
                  remove={(id) => deleteOrder(id)}
                  increment={(id) => increment(id)}
                  decrement={(id) => decrement(id)}
                />
              ))}
            </Flex>
          )}

          {basket && basket.docs.length < 1 && (
            <ButtonAntd
              style={{ width: "100%", marginTop: "30%" }}
              onClick={() => router.push("/")}
              type="link"
            >
              Start shopping
            </ButtonAntd>
          )}

          {basket && basket.docs.length > 0 && (
            <Button
        {total}
      </p>
    );
  };
  return (
    <Page title="Basket" extra={<Bill total={`Ksh. ${2000}`} />}>
      <Container style={{ position: "absolute", top: 56 }}>
        <Flex scrollY style={{ paddingBottom: "5rem" }}>
          {[1, 2, 3, 4, 5].map((product) => (
            <Product
              key={product}
              name="Zesta ketchup sauce - 100g"
              price="200"
              quantity="2"
>>>>>>> 059e6464b35ff52c76cf9295fe4026386275a0c0
              rounded
              large
              yellow
              label="checkout"
              style={{ position: "fixed", bottom: "1.5rem", zIndex: 3 }}
              width="90%"
            />
<<<<<<< HEAD
          )}
        </Container>
      </Page>
    );
  }
}
=======
          ))}
        </Flex>
        <Button
          rounded
          large
          yellow
          label="checkout"
          style={{ position: "fixed", bottom: "1.5rem", zIndex: 3 }}
          width="90%"
        />
      </Container>
    </Page>
  );
}
>>>>>>> 059e6464b35ff52c76cf9295fe4026386275a0c0
