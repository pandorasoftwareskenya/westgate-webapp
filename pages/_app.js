import "../styles/globals.css";
import "antd/dist/antd.css";
import { AuthUserProvider } from "../context";

function MyApp({ Component, pageProps }) {
  return(
  <AuthUserProvider>
    <Component {...pageProps} />
  </AuthUserProvider>);
}

export default MyApp;
