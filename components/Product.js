import Text from "./Text";
import Title from "./Title";
import Button from "./Button";
import { Image, Button as ButtonAntd } from "antd";
import loader from "../public/loader.gif";
import { PlusOutlined, MinusOutlined, CloseOutlined } from "@ant-design/icons";

export default function Product({
  loading,
  add,
  id,
  name,
  price,
  quantity,
  image,
  remove,
  increment,
  decrement,
  basket,
  rounded,
}) {
  return (
    <div
      style={{
        width: "calc(50vw - 32px)",
        margin: "0px 16px 16px 5px",
        background: "#f1f1f1",
        borderRadius: rounded ? "8px" : "0px",
        position: "relative",
      }}
    >
      {basket ? (
        <Button
          onClick={() => remove(id)}
          label={<CloseOutlined />}
          style={{
            zIndex: 2,
            position: "absolute",
            top: 0,
            right: 0,
            height: 36,
          }}
          width={36}
        />
      ) : null}

      <div style={{ padding: 12 }}>
        <Image
          width={`calc(50vw - 56px)`}
          height={`calc(50vw - 56px)`}
          style={{
            objectFit: "cover",
            marginBottom: 12,
            width: "100%",
            border: "none",
          }}
          src={image}
          fallback={loader}
        />

        <Text text={name} />
        <Title text={`Ksh. ${price}`} />
        {basket ? (
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              marginTop: 16,
            }}
          >
            <div style={{ width: 36 }}>
              <Button
                rounded
                label={<MinusOutlined />}
                style={{ height: 36 }}
                full
                onClick={() => decrement(id)}
              />
            </div>
            <div>
              <Title
                style={{ lineHeight: 1, color: "#707070" }}
                text={quantity}
              />
            </div>
            <div style={{ width: 36 }}>
              <Button
                rounded
                label={<PlusOutlined />}
                style={{ height: 36 }}
                full
                onClick={() => increment(id)}
              />
            </div>
          </div>
        ) : (
          <ButtonAntd
            loading={loading}
            onClick={add}
            style={{
              fontFamily: "Metropolis-Regular",
              display: "block",
              fontSize: "1rem",

              backgroundColor: "#3F9B42",
              fontWeight: "900",
              textTransform: "uppercase",
              color: "#fff",
              border: "none",
              borderRadius: "8px",
              width: "100%",
              height: 36,
            }}
            full
          >
            {loading ? "Adding" : "Add"}
          </ButtonAntd>
        )}
      </div>
    </div>
  );
}
