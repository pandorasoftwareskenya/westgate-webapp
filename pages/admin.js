import { Page, Container } from "../components";
import {
  Input,
  Form,
  Typography,
  InputNumber,
  Select,
  Upload,
  message,
  Row,
  Image,
  Col,
  Button,
  Tabs,
  Popconfirm,
  DatePicker,
  Tag,
  Divider,
  Menu,
  Dropdown,
  Modal,
} from "antd";
import { useState } from "react";
import ImgCrop from "antd-img-crop";

import {
  DownOutlined,
  FilterOutlined,
  LoadingOutlined,
  MoreOutlined,
  UserOutlined,
  SearchOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import DB_Services from "../firebase/services";
import Avatar from "antd/lib/avatar/avatar";
import Checkbox from "antd/lib/checkbox/Checkbox";

import { db } from "../firebase/clientApp";
import { collection } from "firebase/firestore";
import { useCollection } from "react-firebase-hooks/firestore";

const { Option } = Select;
const { RangePicker } = DatePicker;
const { TabPane } = Tabs;
const { Title, Text, Paragraph } = Typography;

export default function Admin() {
  const [loading, setLoading] = useState(false);

  // const onChange = ({ fileList: newFileList }) => {
  //   setFileList(newFileList);
  // };
  // const onPreview = async (file) => {
  //   let src = file.url;
  //   if (!src) {
  //     src = await new Promise((resolve) => {
  //       const reader = new FileReader();
  //       reader.readAsDataURL(file.originFileObj);
  //       reader.onload = () => resolve(reader.result);
  //     });
  //   }
  //   const image = new Image();
  //   image.src = src;
  //   const imgWindow = window.open(src);
  //   imgWindow.document.write(image.outerHTML);
  // };

  const [form] = Form.useForm();

  const onSubmit = (values) => {
    //upload image , push data to db
    setLoading(true);

    if (values.category === undefined) {
      values.category = "snacks";
    }
    console.log(values);
    DB_Services.createProduct(values)
      .then(() => {
        message.success("Sucess");
        setLoading(false);
        form.resetFields();
      })
      .catch((err) => {
        message.error("Error");
        setLoading(false);
        form.resetFields();
      });
  };

  const onCategoryChange = (value) => {
    switch (value) {
      case "snacks":
        form.setFieldsValue({
          category: "snacks",
        });
        return;

      case "other":
        form.setFieldsValue({
          category: "other",
        });
    }
  };

  // return (
  //   <Page title="Admin">
  //     <Container style={{ position: "absolute", top: 56 }}>
  //       <Form onFinish={onSubmit}>
  //         <Form.Item label="Product name" name="name">
  //           <Input placeholder="name" />
  //         </Form.Item>
  //         <Form.Item label="Product price" name="price">
  //           <Input
  //             addonBefore="KES"
  //             style={{ height: 30 }}
  //             placeholder="Price"
  //           />
  //         </Form.Item>
  //         <Form.Item label="Product quantity" name="quantity">
  //           <Input
  //             addonAfter="Pcs"
  //             style={{ height: 30 }}
  //             placeholder="Quantity"
  //           />
  //         </Form.Item>
  //         <Form.Item label="Category" name="category">
  //           <Select
  //             onChange={onCategoryChange}
  //             defaultValue="snacks"
  //             style={{ width: "100%" }}
  //           >
  //             <Option value="snacks" key="1">
  //               Snacks
  //             </Option>
  //             <Option value="other" key="2">
  //               Other
  //             </Option>
  //           </Select>
  //         </Form.Item>
  //         {/* <ImgCrop rotate>
  //           <Upload
  //             action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
  //             listType="picture-card"
  //             fileList={fileList}
  //             onChange={onChange}
  //             onPreview={onPreview}
  //           >
  //             {fileList.length < 1 && "+ Upload"}
  //           </Upload>
  //         </ImgCrop> */}
  //         <Form.Item>
  //           <Button type="primary" htmlType="submit" loading={loading}>
  //             Upload
  //           </Button>
  //         </Form.Item>
  //       </Form>
  //     </Container>
  //   </Page>
  // );

  return <Dashboard />;
}

const Dashboard = () => {
  const signOut = () => {
    console.log("sign out");
  };
  const SignOutBtn = () => {
    return (
      <Popconfirm
        onConfirm={signOut}
        cancelText="No"
        okText="Yes"
        title="Are you sure?"
      >
        <Button type="link" style={{ color: "#E5E055" }}>
          Sign out
        </Button>
      </Popconfirm>
    );
  };

  const ProductsComponent = (product) => {
    const [addModal, setAddModal] = useState(false);
    const [productName, setProductName] = useState("");
    const [productPrice, setProductPrice] = useState();
    const [productQuantity, setProductQuantity] = useState();
    const [category, setCategory] = useState("snacks");
    const [imageLoading, setImageLoading] = useState(false);
    const [imageUrl, setImageURL] = useState();
    const [uploadLoading, setLoadingUpload] = useState(false);

    const [products, productsLoading, productsError] = useCollection(
      collection(db, "products"),
      {}
    );

    const handleOkAdd = () => {
      if (!productName) {
        message.warning("Product name required to save image!");
      } else {
        setLoadingUpload(true);
        DB_Services.uploadProductImage(productName, imageUrl)
          .then((productImageURL) => {
            let _product = {
              name: productName,
              price: productPrice,
              quantity: productQuantity,
              category,
              image: productImageURL,
            };
            console.log(_product);
            DB_Services.createProduct(_product)
              .then(() => {
                message.success(
                  `Successfully added '${productName}' to the database`
                );
              })
              .then(() => setLoadingUpload(false))
              .then(() => {
                setAddModal(false);
                setCategory("snacks");
                setProductQuantity(null);
                setProductName(null);
                setProductPrice(null);
              })
              .catch((err) => message.error("Oops! Server error"));
          })
          .catch((err) => message.error("Failed to upload product image"));
      }
    };

    function getBase64(img, callback) {
      const reader = new FileReader();
      reader.addEventListener("load", () => callback(reader.result));
      reader.readAsDataURL(img);
    }

    function beforeUpload(file) {
      const isJpgOrPng =
        file.type === "image/jpeg" || file.type === "image/png";
      if (!isJpgOrPng) {
        message.error("You can only upload JPG/PNG file!");
      }
      const isLt2M = file.size / 1024 / 1024 < 2;
      if (!isLt2M) {
        message.error("Image must smaller than 2MB!");
      }
      return isJpgOrPng && isLt2M;
    }

    const handleChangeImage = (info) => {
      if (info.file.status === "uploading") {
        setImageLoading(true);
        return;
      }
      if (info.file.status === "done") {
        // Get this url from response in real world.
        getBase64(info.file.originFileObj, (imageUrl) => {
          console.log(imageUrl);
          setImageURL(imageUrl);
          setImageLoading(false);
        });
      }
    };

    const [keyword, setKeyword] = useState("");

    const AdminProduct = (data) => {
      const [offersModal, setOffersModal] = useState(false);
      const [editModal, setEditModal] = useState(false);
      const [offer_price, setOfferPrice] = useState();
      const [startDate, setStartDate] = useState();
      const [endDate, setEndDate] = useState();
      const [loading, setLoading] = useState(false);
      const [loadingEdit, setLoadingEdit] = useState(false);
      const [editedPrice, setEditedPrice] = useState(data.data.price);
      const [editedName, setEditedName] = useState(data.data.name);
      const [editedCategory, setEditedCategory] = useState(data.data.category);
      const [editedQuantity, setEditedQuantity] = useState(data.data.quantity);

      const addToOffers = () => {
        setOffersModal(true);
      };

      const confirmDelete = () => {
        DB_Services.deleteProduct({ id: data.id })
          .then(() => message.success("Product deleted"))
          .catch((err) => console.log(err));
      };

      const handleOkOffers = () => {
        if (offer_price == null || startDate == null || endDate == null) {
          message.warning("Please fill in the missing field");
        } else {
          setLoading(true);
          let update = {
            offer_price,
            startDate,
            endDate,
            id: data.id,
            on_offer: true,
          };
          DB_Services.addOffer(update)
            .then(() =>
              message.success(
                `Successfully added '${data.data.name}' to offers!`
              )
            )
            .then(() => setLoading(false))
            .then(() => setOffersModal(false))
            .catch(() => message.error("Oops! Failed. Try again later"));
        }
      };

      const handleOkEdit = () => {
        setLoadingEdit(true);
        let _product = {
          id: data.id,
          name: editedName,
          price: editedPrice,
          category: editedCategory,
          quantity: editedQuantity,
        };
        DB_Services.updateProduct(_product)
          .then(() => message.success("Updated!"))
          .then(() => setLoadingEdit(false))
          .then(() => setEditModal(false));
      };

      const handleCancel = () => {
        setOffersModal(false);
      };

      const menu = (
        <Menu>
          <Menu.Item key="0">
            <a onClick={addToOffers}>Add to offers</a>
          </Menu.Item>
          <Menu.Divider />
          <Menu.Item key="3">
            <a onClick={() => setEditModal(true)}>Edit product</a>
          </Menu.Item>
          <Menu.Divider />
          <Menu.Item key="4">
            <Popconfirm
              title="Are you sure to delete this product?"
              onConfirm={confirmDelete}
              okText="Yes"
              cancelText="No"
            >
              <a>Delete product</a>
            </Popconfirm>
          </Menu.Item>
        </Menu>
      );

      return (
        <div
          style={{
            width: "100%",
            marginBottom: 0,
            zIndex: 99,
          }}
        >
          <Row
            style={{
              position: "relative",
              background: "#f1f1f1",
              marginBottom: 8,
              padding: 8,
              height: 116,
            }}
          >
            {data.data.on_offer && (
              <Tag
                color="yellow"
                style={{ position: "absolute", top: 0, left: 0, zIndex: 2 }}
              >
                OFFER
              </Tag>
            )}

            <Col span={8}>
              <Image
                src={data.data.image || ""}
                width={100}
                height={100}
                style={{ objectFit: "cover" }}
              />
            </Col>
            <Col span={13}>
              <div style={{ maxHeight: 150 }}>
                <p style={{ display: "block", margin: "2px 0px" }}>
                  <strong>{data.data.name}</strong>
                </p>
                <p
                  style={{
                    display: "block",
                    margin: "8px 0px",
                    color: "#707070",
                  }}
                >
                  Ksh. {data.data.price}
                </p>
                <span>
                  <Text color="#E5E055" style={{ marginRight: 12 }}>
                    {data.data.quantity}
                  </Text>
                  <Tag
                    color={
                      data.data.category == "snacks"
                        ? "orange"
                        : data.data.category == "drinks"
                        ? "purple"
                        : data.data.category == "ingredients"
                        ? "green"
                        : data.data.category == "cleaning"
                        ? "blue"
                        : data.data.category == "toiletries"
                        ? "skyblue"
                        : data.data.category == "other"
                        ? "brown"
                        : "pink"
                    }
                  >
                    {data.data.category.toString().toUpperCase()}
                  </Tag>
                </span>
              </div>
            </Col>
            <Col span={1}>
              <Dropdown overlay={menu} trigger={["click"]}>
                <Button type="link" color="#707070">
                  <MoreOutlined />
                </Button>
              </Dropdown>
            </Col>
          </Row>
          <Modal
            title="Edit product"
            onOk={handleOkEdit}
            visible={editModal}
            onCancel={() => setEditModal(false)}
            okText={loadingEdit ? "Updating" : "Update"}
            confirmLoading={loadingEdit}
          >
            <Form>
              <Form.Item label="Product name">
                <Input
                  value={editedName}
                  onChange={(e) => setEditedName(e.target.value)}
                  size="large"
                />
              </Form.Item>
              <Form.Item label="Price">
                <InputNumber
                  addonBefore="KSH"
                  value={editedPrice}
                  style={{ width: "100%" }}
                  size="large"
                  onChange={(val) => setEditedPrice(val)}
                />
              </Form.Item>
              <Form.Item label="Category">
                <Select
                  style={{ width: "100%" }}
                  size="large"
                  defaultValue={editedCategory}
                  onChange={(category) => setEditedCategory(category)}
                >
                  {[
                    "snacks",
                    "drinks",
                    "ingredients",
                    "cleaning",
                    "toiletries",
                    "other",
                  ].map((category, index) => (
                    <Option key={category}>{category}</Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item label="Quantity">
                <InputNumber
                  value={editedQuantity}
                  style={{ width: "100%" }}
                  size="large"
                  onChange={(val) => setEditedQuantity(val)}
                />
              </Form.Item>
            </Form>
          </Modal>
          <Modal
            visible={offersModal}
            title="Add To Offers"
            onOk={handleOkOffers}
            onCancel={handleCancel}
            confirmLoading={loading}
            okText={loading ? "Adding" : "Save"}
          >
            <Form>
              <Form.Item label="Offer price" name="offer_price">
                <InputNumber
                  addonBefore="KSH"
                  placeholder="Offer price"
                  value={offer_price}
                  onChange={(val) => setOfferPrice(val)}
                />
              </Form.Item>
              <Form.Item label="Offer validity" name="offer_validity">
                <RangePicker
                  showTime
                  style={{ width: "100%" }}
                  onCalendarChange={(val, dateString) => {
                    setStartDate(dateString[0]);
                    setEndDate(dateString[1]);
                  }}
                />
              </Form.Item>
            </Form>
          </Modal>
        </div>
      );
    };

    const uploadButton = (
      <div style={{ width: "100%" }}>
        {imageLoading ? <LoadingOutlined /> : <PlusOutlined />}
        <div style={{ marginTop: 8 }}>Upload</div>
      </div>
    );

    return (
      <>
        <Row style={{ width: "100%" }}>
          <Col span={22}>
            <Input
              value={keyword}
              placeholder="Search product..."
              suffix={<SearchOutlined />}
              onChange={(e) => setKeyword(e.target.value)}
            />
          </Col>
          <Col span={2}>
            <Button type="link" style={{ color: "#707070" }}>
              <FilterOutlined />
            </Button>
          </Col>
        </Row>
        <br />
        {!products && productsLoading && <p>Loading...</p>}
        {products &&
          products.docs.length > 0 &&
          products.docs
            .filter((prod) =>
              prod.data().name.toLowerCase().includes(keyword.toLowerCase())
            )
            .map((product) => {
              return <AdminProduct data={product.data()} id={product.id} />;
            })}
        {products && products.docs.length < 1 && <p>Add products</p>}
        <Button
          onClick={() => setAddModal(true)}
          style={{
            position: "fixed",
            bottom: "3rem",
            right: "0.5rem",
            width: 48,
            height: 48,
            borderRadius: "50%",
            background: "#3F9B42",
          }}
        >
          <PlusOutlined style={{ color: "white" }} />
        </Button>
        <Modal
          title="Add product"
          visible={addModal}
          okText={uploadLoading ? "Uploading" : "Save"}
          onOk={handleOkAdd}
          onCancel={() => setAddModal(false)}
          confirmLoading={uploadLoading}
        >
          <Form>
            <Form.Item label="Product name">
              <Input
                value={productName}
                size="large"
                onChange={(e) => setProductName(e.target.value)}
                placeholder="Name"
              />
            </Form.Item>
            <Form.Item label="Price">
              <InputNumber
                size="large"
                style={{ width: "100%" }}
                onChange={(val) => setProductPrice(val)}
                addonBefore="KSH"
                placeholder="Price"
                value={productPrice}
              />
            </Form.Item>
            <Form.Item label="Quantity">
              <InputNumber
                size="large"
                style={{ width: "100%" }}
                onChange={(val) => setProductQuantity(val)}
                placeholder="Quantity"
                value={productQuantity}
              />
            </Form.Item>
            <Form.Item label="Category">
              <Select
                style={{ width: "100%" }}
                size="large"
                defaultValue="snacks"
                onChange={(category) => setCategory(category)}
              >
                {[
                  "snacks",
                  "drinks",
                  "ingredients",
                  "cleaning",
                  "toiletries",
                  "other",
                ].map((category, index) => (
                  <Option key={category}>{category}</Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item label="Product Image">
              <ImgCrop rotate style={{ width: "100vw" }}>
                <Upload
                  name="avatar"
                  listType="picture-card"
                  className="avatar-uploader"
                  showUploadList={false}
                  beforeUpload={beforeUpload}
                  onChange={handleChangeImage}
                >
                  {imageUrl ? (
                    <img
                      src={imageUrl}
                      alt="avatar"
                      style={{
                        width: 150,
                        height: 150,
                        objectFit: "cover",
                        margin: "36px 0px 0px 36px",
                      }}
                    />
                  ) : (
                    uploadButton
                  )}
                </Upload>
              </ImgCrop>
            </Form.Item>
          </Form>
        </Modal>
      </>
    );
  };

  const OrdersComponent = () => {
    const [keyword, setKeyword] = useState("");

    const onChangePacked = (ids) => {
      console.log("Packed!");
    };

    const onChangeDelivered = (ids) => {
      console.log("Delivered!");
    };

    const AdminOrder = ({ isVisible }) => {
      return (
        <div style={{ width: "100%", marginBottom: 12 }}>
          <Row>
            <Col span={4} offset={1}>
              <Avatar
                style={{ backgroundColor: "#3F9B42" }}
                icon={<UserOutlined />}
              />
            </Col>
            <Col span={10}>
              <Text style={{ display: "block", fontSize: "1.1rem" }} strong>
                Kinut
              </Text>
              <Text>12-Dec-2021</Text>
            </Col>
            <Col span={6}>
              <Tag color="green" style={{ float: "right" }}>
                PAID
              </Tag>
            </Col>
            <Col span={2}>
              <Button type="link" color="#707070">
                <DownOutlined />
              </Button>
            </Col>
          </Row>
          <Container
            style={{
              display: isVisible ? "block" : "none",
              background: "#f1f1f1",
            }}
          >
            <Divider orientation="right">Products</Divider>
            {[1, 2, 3, 4, 5].map((product) => {
              return (
                <Row style={{ marginBottom: 6 }}>
                  <Col span={10}>Salt -100g</Col>
                  <Col span={2} offset={1}>
                    2
                  </Col>
                  <Col span={1} offset={1}>
                    @
                  </Col>
                  <Col span={3} offset={1}>
                    200
                  </Col>
                  <Col span={4} offset={1}>
                    <strong>400</strong>
                  </Col>
                </Row>
              );
            })}

            <p style={{ width: "100%", textAlign: "right", padding: 12 }}>
              Tot : <strong>Ksh. 4000</strong>
            </p>
            <Divider />
            <Checkbox onChange={onChangePacked}>Packed</Checkbox>
            <Checkbox onChange={onChangeDelivered}>Delivered</Checkbox>
          </Container>
          <Divider />
        </div>
      );
    };
    return (
      <>
        <Row style={{ width: "100%" }}>
          <Col span={23}>
            <Input
              value={keyword}
              placeholder="Search order..."
              suffix={<SearchOutlined />}
              onChange={(e) => setKeyword(e.target.value)}
            />
          </Col>
          <Col span={1}>
            <Button type="link" style={{ color: "#3F9B42" }}>
              <FilterOutlined />
            </Button>
          </Col>
        </Row>
        <br />
        <div
          style={{
            maxHeight: "calc(100vh - 200px)",
            overflow: "scroll",
            width: "100%",
          }}
        >
          {[1, 2, 3, 4].map((order) => {
            return <AdminOrder isVisible={false} />;
          })}
        </div>
      </>
    );
  };

  const AdminsComponent = () => {
    return (
      <Container>
        {[1, 1, 1].map((admin) => {
          return (
            <>
              <Row>
                <Col span={12}>Wilson Kinyanjui</Col>
                <Col>
                  <a>wilsonk</a>
                </Col>
              </Row>
              <Divider />
            </>
          );
        })}
      </Container>
    );
  };

  return (
    <>
      <Page title="Admin" extra={<SignOutBtn />}>
        <Container
          style={{
            position: "absolute",
            top: 56,
            width: "95%",
            minHeight: "95vh",
          }}
        >
          <Tabs defaultActiveKey="1">
            <TabPane tab="Dashboard" key="1"></TabPane>
            <TabPane tab="Products" key="2">
              <ProductsComponent />
            </TabPane>
            <TabPane tab="Orders" key="3">
              <OrdersComponent />
            </TabPane>
            <TabPane tab="Admins" key="4">
              <AdminsComponent />
            </TabPane>
          </Tabs>
        </Container>
      </Page>
    </>
  );
};
