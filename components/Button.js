export default function Button({
  rounded,
  style,
  large,
  yellow,
  label,
  full,
  width,
  onClick,
}) {
  return (
    <button
      style={{
        ...style,
        fontFamily: large ? "Metropolis-Bold" : "Metropolis-Regular",
        display: "block",
        fontSize: "1rem",
        padding: large ? "0.9rem" : "0.5rem",
        backgroundColor: yellow ? "#E5E055" : "#3F9B42",
        fontWeight: "900",
        textTransform: "uppercase",
        color: yellow ? "#3F9B42" : "#fff",
        border: "none",
        borderRadius: rounded ? "8px" : "0px",
        width: full ? "100%" : width,
      }}
      onClick={onClick}
    >
      {label}
    </button>
  );
}
