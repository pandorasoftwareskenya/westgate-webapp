import {
  Container,
  Product,
  Button,
  Title,
  Search,
  Category,
  Text,
  Flex,
  Page,
  SkeletonCustom,
} from "../components";
import { message } from "antd";
import { useState } from "react";
import { db } from "../firebase/clientApp";
import DB_Services from "../firebase/services";

import { useRouter } from "next/router";
import { collection, query, where } from "firebase/firestore";
import { useCollection } from "react-firebase-hooks/firestore";

export default function Group() {
  let user = "1";

  const [id, setId] = useState(null);

  const router = useRouter();

  const { category } = router.query;

  const [products, productsLoading, productsError] = useCollection(
    query(collection(db, "products"), where("category", "==", category)),
    {}
  );

  const [basket, basketLoading, basketError] = useCollection(
    query(collection(db, "orders"), where("user.id", "==", user)),
    {}
  );

  const increment = (id) => {
    DB_Services.incrementOrder({ id })
      .then(() => console.log("incremented"))
      .catch((err) => console.log(err));
  };

  const decrement = (id) => {
    if (basket.docs.filter((order) => order.id == id)[0].data().quantity == 1) {
      deleteOrder(id);
    }
    DB_Services.decrementOrder({ id })
      .then(() => console.log("decremented"))
      .catch((err) => console.log(err));
  };

  const deleteOrder = (id) => {
    DB_Services.deleteOrder({ id })
      .then(() => message.success("Item removed from cart"))
      .catch((err) => console.log(err));
  };

  const addToCart = (product) => {
    setId(product.id);

    DB_Services.createOrder({
      product: {
        id: product.id,
        name: product.data().name,
        image: product.data().image,
        price: product.data().price,
      },
      user: {
        id: "1",
        name: "Kinut",
        phoneNumber: "748920306",
      },
    })
      .then(() => {
        message.success("Added to cart");
        setId(null);
      })
      .catch((err) => {
        message.error("Failed");
        setId(null);
      });
  };

  if (!category) return <p>Loading...</p>;

  return (
    <Page title={category}>
      <Container style={{ position: "absolute", top: 56 }}>
        {productsError && <p>Error...</p>}

        {productsLoading ||
          (basketLoading && (
            <Flex scrollY style={{ paddingBottom: "5rem" }}>
              {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((el) => (
                <SkeletonCustom key={el} />
              ))}
            </Flex>
          ))}
        {productsLoading && <p>Loading...</p>}
        {basketLoading && <p>Basket loading...</p>}

        {basket && products && products.docs.length > 0 && (
          <Flex scrollY style={{ paddingBottom: "5rem" }}>
            {products.docs.map((product) => (
              <Product
                loading={id == product.id ? true : false}
                name={product.data().name}
                price={product.data().price}
                add={() => addToCart(product)}
                quantity={
                  basket.docs.filter(
                    (order) => order.data().product.id == product.id
                  ).length > 0
                    ? basket.docs
                        .filter(
                          (order) => order.data().product.id == product.id
                        )[0]
                        .data().quantity
                    : null
                }
                rounded
                basket={
                  basket.docs.filter(
                    (order) => order.data().product.id == product.id
                  ).length > 0
                    ? true
                    : false
                }
                id={product.id}
                remove={() =>
                  deleteOrder(
                    basket.docs.filter(
                      (order) => order.data().product.id == product.id
                    )[0].id
                  )
                }
                increment={() =>
                  increment(
                    basket.docs.filter(
                      (order) => order.data().product.id == product.id
                    )[0].id
                  )
                }
                decrement={() =>
                  decrement(
                    basket.docs.filter(
                      (order) => order.data().product.id == product.id
                    )[0].id
                  )
                }
              />
            ))}
          </Flex>
        )}
      </Container>
    </Page>
  );
}
