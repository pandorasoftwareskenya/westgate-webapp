import Title from "./Title";

export default function Page({ children, title, style, extra }) {
  return (
    <div
      style={{
        width: "100vw",
        maxWidth: "100vw",
        minHeight: "100vh",
        height: "100vh",
        overflowX: "hidden",
        ...style,
      }}
    >
      {title ? (
        <div
          style={{
            background: "#3F9B42",
            height: 56,
            width: "100%",
            padding: 12,
            position: "fixed",
            zIndex: 3,
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <p
            style={{
              color: "#fff",
              textTransform: "capitalize",
              lineHeight: 1.5,
              fontSize: "1.3rem",
              fontFamily: "Metropolis-SemiBold",
            }}
          >
            {title}
          </p>
          {extra ? extra : null}
        </div>
      ) : null}
      {children}
    </div>
  );
}
