import {
  collection,
  addDoc,
  doc,
  increment,
  deleteDoc,
  updateDoc,
  setDoc,
} from "firebase/firestore";
import { ref, uploadString, getDownloadURL } from "firebase/storage";
import { db, storage } from "./clientApp";

const productsRef = collection(db, "products");
const ordersRef = collection(db, "orders");
const usersRef = collection(db, "users");

const uploadProductImage = (name, dataURL) => {
  return new Promise((resolve, reject) => {
    const storageRef = ref(storage, "products/" + `${name}`);

    uploadString(storageRef, dataURL, "data_url")
      .then((snapshot) => {
        if (snapshot) {
          const progress =
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100;

          switch (snapshot.state) {
            case "paused":
              console.log("Upload is paused");
              break;
            case "running":
              console.log("Upload is running");
              break;
          }

          return snapshot.ref;
        }
      })
      .then((ref) =>
        getDownloadURL(ref)
          .then((downloadURL) => {
            resolve(downloadURL);
            console.log("File available at", downloadURL);
          })
          .catch((err) => reject(err))
      );
  });
};

// const getAll = () => {
//   return db;
// };

const createProduct = async (args) => {
  const docRef = await addDoc(productsRef, {
    name: args.name,
    price: args.price,
    quantity: args.quantity,
    category: args.category,
    image: args.image,
    offer_price: null,
    on_offer: false,
    added: Date.now(),
  });

  return docRef;
};

const createUser = async (args) => {
  await setDoc(doc(db, "users", args.uid), {
    displayName: args.displayName,
    email: args.email,
    phoneNumber: args.phoneNumber,
    photo: args.photo == null ? null : args.photo,
  });
};

const addOffer = async (args) => {
  const { offer_price, startDate, endDate, on_offer } = args;
  let data = {
    offer_price,
    start_date: startDate,
    end_date: endDate,
    on_offer,
  };
  await updateDoc(doc(db, "products", `${args.id}`), data);
};

const updateProduct = async (args) => {
  const { id, name, price, category, quantity } = args;
  let data = {
    name,
    price,
    category,
    quantity,
  };
  await updateDoc(doc(db, "products", `${id}`), data);
};

const deleteProduct = async (args) => {
  await deleteDoc(doc(db, "products", args.id));
};

const createOrder = async (args) => {
  const docRef = await addDoc(ordersRef, {
    added: Date.now(),
    delivered: false,
    paid: false,
    product: args.product,
    quantity: 1,
    user: args.user,
  });

  console.log(docRef);

  return docRef;
};

const incrementOrder = async (args) => {
  const docRef = await updateDoc(doc(db, "orders", args.id), {
    quantity: increment(1),
  });
  return docRef;
};

const decrementOrder = async (args) => {
  const docRef = await updateDoc(doc(db, "orders", args.id), {
    quantity: increment(-1),
  });
  return docRef;
};

const deleteOrder = async (args) => {
  const docRef = await deleteDoc(doc(db, "orders", args.id));
  return docRef;
};

// const update = (id, value) => {
//   return db.doc(id).update(value);
// };

// const remove = (id) => {
//   return db.doc(id).delete();
// };

const DB_Services = {
  createProduct,
  createOrder,
  incrementOrder,
  decrementOrder,
  deleteOrder,
  updateProduct,
  uploadProductImage,
  deleteProduct,
  addOffer,
  createUser,
  productsRef,
};

export default DB_Services;

// createProduct({ name: "Test", price: 2000, quantity: 2, category: "cat" });
