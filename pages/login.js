import { Button, message, Image } from "antd";
import { doc, getDoc } from "firebase/firestore";
import { useAuth } from "../context";
import { useRouter } from "next/router";
import authenticate from "../public/authenticate.png";
import DB_Services from "../firebase/services";
import { useState } from "react";
import { db } from "../firebase/clientApp";

export default function Login() {
  const { signInFirebase } = useAuth();
  const router = useRouter();
  const [loading, setLoading] = useState(false);

  const signIn = async () => {
    setLoading(true);
    signInFirebase().then((authUser) => {
      const docRef = doc(db, "users", authUser.uid);

      getDoc(docRef).then((docSnap) => {
        if (docSnap.exists()) {
          message.success(`Welcome back , ${authUser.displayName}`);
          setLoading(false);
          router.push("/");
        } else {
          DB_Services.createUser(authUser)
            .then(() => {
              setLoading(false);
              message.success(`Welcome to the squad ${authUser.displayName}!`);
            })
            .then(() => router.push("/"))
            .catch((err) => {
              setLoading(false);
              console.log(err);
            });
        }
      });
    });
  };

  return (
    <div>
      <img
        src={authenticate.src}
        width={300}
        height={178.1}
        style={{
          position: "absolute",
          top: "20%",
          left: "3rem",
        }}
      />
      <p
        style={{
          width: "100%",
          textAlign: "center",
          color: "#707070",
          position: "absolute",
          top: "50%",
        }}
      >
        It's time we knew who you are!
      </p>
      <Button
        style={{
          width: "90%",
          left: "5%",
          position: "fixed",
          bottom: "7rem",
          height: "3rem",
        }}
        size="large"
        onClick={signIn}
        loading={loading}
      >
        <img
          style={{ marginRight: "1rem" }}
          src="https://img.icons8.com/fluency/24/000000/google-logo.png"
        />
        Sign in with Google
      </Button>
    </div>
  );
}
