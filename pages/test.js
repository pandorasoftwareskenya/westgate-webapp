import SkeletonCustom from "../components/Skeleton";
import { useAuth } from "../context";
import { useEffect } from "react";

export default function Test() {
  const { authUser, loading } = useAuth();
  useEffect(() => {
    if (!loading && !authUser) router.push("/");
  }, [authUser, loading]);
  return <SkeletonCustom />;
}
