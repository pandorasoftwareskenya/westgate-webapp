import { Page, Container } from "../components";
import { useRouter } from "next/router";
import { useState } from "react";
import { Image, Avatar, Form, Input, message, Skeleton } from "antd";
import { UserOutlined } from "@ant-design/icons/lib/icons";
import { useAuth } from "../context";
import { db } from "../firebase/clientApp";

import { doc, getDoc } from "firebase/firestore";

export default function Account() {
  const router = useRouter();
  const { authUser, loading, _signOut } = useAuth();
  const [signedIn, setSignedIn] = useState(true);
  const [uid, setUID] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [photoURL, setPhotoURL] = useState("");
  const [val, setVal] = useState(0);

  const getUserDetails = async (id) => {
    return new Promise((resolve, reject) => {
      getDoc(doc(db, "users", id)).then((docSnap) => {
        if (docSnap.exists()) {
          resolve(docSnap);
        } else {
          reject("No document found");
        }
      });
    });
  };

  if (loading) return <p>Loading....</p>;

  if (authUser) {
    getUserDetails(authUser.uid)
      .then((user) => {
        setUID(user.id);
        setName(user.data().displayName);
        setEmail(user.data().email);
        setPhone(user.data().phoneNumber);
        setPhotoURL(user.data().photo);
      })
      .catch((err) => console.log(err));

    const onSubmit = () => {
      console.log("Submit");
    };

    const Logout = () => {
      return (
        <button
          onClick={() => {
            _signOut().then((res) => {
              if (res) {
                message.info("Logged out");
                router.push("/login");
              }
            });
          }}
          style={{
            border: "none",
            outline: "none",
            background: "transparent",
            textDecoration: "underline",
            color: "#E5E055",
            fontSize: "0.9rem",
            zIndex: 4,
          }}
        >
          Sign out
        </button>
      );
    };

    console.log(name);

    return (
      <Page title="Account" extra={<Logout />}>
        <Container style={{ position: "absolute", top: 56, width: "95%" }}>
          <br />
          {photoURL ? (
            <Image
              width={150}
              height={150}
              src={photoURL}
              style={{
                objectFit: "cover",
                borderRadius: 75,
                display: "block",
                margin: "0 auto",
              }}
            />
          ) : (
            <Avatar
              size={150}
              style={{
                backgroundColor: "#3F9B42",
                display: "block",
                margin: "0 auto",
              }}
              icon={<UserOutlined />}
            />
          )}
          <Form onFinish={onSubmit}>
            <Form.Item label="Name" name="name">
              {!authUser || loading ? (
                <Skeleton.Button active block size="large" />
              ) : (
                <Input
                  placeholder="Your name"
                  size="large"
                  value={authUser.displayName}
                />
              )}
            </Form.Item>
            <Form.Item label="Phone number" name="telephone">
              <Input
                addonBefore="+254"
                placeholder="Your phone number"
                size="large"
                value={phone}
              />
            </Form.Item>
            <Form.Item label="Email" name="email">
              <Input
                placeholder="Your email address"
                size="large"
                value={email}
                addonAfter="@gmail.com"
              />
            </Form.Item>
            <Form.Item label="Location" name="address">
              <Input placeholder="Building , House no." size="large" />
            </Form.Item>
          </Form>
        </Container>
      </Page>
    );
  }
}
