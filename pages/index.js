import { useState, useEffect } from "react";

import { app, db } from "../firebase/clientApp";

import DB_Services from "../firebase/services";
import { collection, query, where } from "firebase/firestore";
import { useCollection } from "react-firebase-hooks/firestore";
import {
  Container,
  Product,
  Button,
  Title,
  Search,
  Category,
  Text,
  Flex,
  Page,
  SkeletonCustom,
} from "../components";

import { Badge, Avatar, Carousel, message } from "antd";
import { ShoppingOutlined, UserOutlined } from "@ant-design/icons/lib/icons";

import { useAuth } from "../context";
import { useRouter } from "next/router";

export default function Home() {
  const [signedIn, setSignedIn] = useState(true);
  const { authUser, loading } = useAuth();
  const router = useRouter();

  const [basket, basketLoading, basketError] = useCollection(
    query(
      collection(db, "orders"),
      where("user", "==", authUser ? authUser.uid : "unfindalble")
    ),
    {}
  );

  useEffect(() => {
    if (!loading && !authUser) setSignedIn(false);
  }, [authUser, loading]);

  const goToAccount = () => {
    if (!signedIn) {
      router.push("/login");
    } else {
      router.push("/account");
    }
  };

  const Header = () => {
    const [keyword, setKeyword] = useState("");

    return (
      <>
        <row style={{ display: "flex" }}>
          <Search
            rounded
            style={{ width: "75%" }}
            placeholder="Search eggs , ketchup..."
            value={keyword}
            onChange={(e) => setKeyword(e.target.value)}
          />
          <div style={{ height: "inherit", paddingTop: 6, display: "flex" }}>
            <button
              style={{
                height: 40,
                margin: 0,
                outline: "none",
                border: "none",
                background: "white",
                display: "inline",
                top: 12,
              }}
              onClick={() => {
                if (!signedIn) {
                  router.push("/login");
                } else {
                  router.push("/basket");
                }
              }}
            >
              <Badge color="#3F9B42" count={basket ? basket.docs.length : 0}>
                <ShoppingOutlined
                  style={{ fontSize: "1.5rem", color: "#707070" }}
                />
              </Badge>
            </button>
            <button
              style={{
                height: 40,
                margin: 0,
                outline: "none",
                border: "none",
                background: "white",
                display: "inline",
                top: 12,
              }}
              onClick={goToAccount}
            >
              <Avatar
                style={{ backgroundColor: "#3F9B42" }}
                icon={<UserOutlined />}
              />
            </button>
          </div>
        </row>
        <br />
      </>
    );
  };

  const Offers = () => {
    const contentStyle = {
      height: "160px",
      color: "#fff",
      lineHeight: "160px",
      textAlign: "center",
      background: "#364d79",
      width: "calc(100vw - 32px)",
    };

    return (
      <Carousel autoplay>
        <div>
          <h3 style={contentStyle}>1</h3>
        </div>
        <div>
          <h3 style={contentStyle}>2</h3>
        </div>
        <div>
          <h3 style={contentStyle}>3</h3>
        </div>
        <div>
          <h3 style={contentStyle}>4</h3>
        </div>
      </Carousel>
    );
  };

  const Categories = () => {
    return (
      <>
        <Title text="Categories" />
        <br />
        <Flex scrollX>
          {[
            {
              icon: (
                <svg
                  width="24"
                  height="24"
                  xmlns="http://www.w3.org/2000/svg"
                  fill-rule="evenodd"
                  clip-rule="evenodd"
                >
                  <path d="M18 23h-12l-2.604-12 .003.002-1.399-3.575 3.022-1.732-1.022-4.695 4.802.004.188.996h4.685l.216-1 4.745 1-.579 2.612 3.943 2.388-1.439 4.234-2.561 11.766zm-1.67-2l1.828-7.966s-2.777 1.214-6.158 1.205c-3.381-.009-6.162-1.191-6.162-1.191l1.825 7.951v.001h8.667zm0 0h.008l.005-.021-.013.021zm-4.105-8.774v-5.15l-1.873-.384v2.507l.651 3.006c.406.024.813.031 1.222.021zm1.916-.176c.582-.091 1.167-.217 1.755-.379l.279-2.018-1.672-.231-.362 2.628zm-6.285-.387c.536.154 1.074.276 1.616.368l-1.913-9.528-1.697-.001 1.994 9.161zm11.411-1.286l.925-2.719-1.477-.894-1.104 3.451-.129.939c.592-.224 1.187-.483 1.785-.777zm-13.072.707l-.551-2.53-.434-1.238-1.342.769.918 2.346c.468.242.937.46 1.409.653zm7.754-3.253l1.803.249 1.092-4.925-1.802-.38-1.093 5.056zm-1.604-4.331h-2.195v1.619l2.195.45v-2.069z" />
                </svg>
              ),
              label: "Snacks",
            },
            {
              icon: (
                <svg
                  width="24"
                  height="24"
                  xmlns="http://www.w3.org/2000/svg"
                  fill-rule="evenodd"
                  clip-rule="evenodd"
                >
                  <path d="M12.185 4l2.113-4 1.359.634-1.817 3.366h4.199c0 .922 1.092 1.618 1.961 1.618v1.382h-1l-2 17h-10l-2.02-17h-.98v-1.382c.87 0 2-.697 2-1.618h6.185zm-5.185 3l1.735 15h6.53l1.735-15h-10zm3.75 10.5c.414 0 .75.336.75.75s-.336.75-.75.75-.75-.336-.75-.75.336-.75.75-.75zm2.727-2.5c.552 0 1 .448 1 1s-.448 1-1 1-1-.448-1-1 .448-1 1-1zm-2.977-3c.828 0 1.5.672 1.5 1.5s-.672 1.5-1.5 1.5-1.5-.672-1.5-1.5.672-1.5 1.5-1.5z" />
                </svg>
              ),
              label: "Drinks",
            },
            {
              icon: (
                <img
                  style={{ height: 23, width: 23 }}
                  src="https://img.icons8.com/material-outlined/50/000000/ingredients.png"
                />
              ),
              label: "Ingredients",
            },
            {
              icon: (
                <img src="https://img.icons8.com/external-flatart-icons-outline-flatarticons/22/000000/external-cleaning-hygiene-routine-flatart-icons-outline-flatarticons.png" />
              ),
              label: "Cleaning",
            },
            {
              icon: (
                <img src="https://img.icons8.com/ios/22/000000/toilet-paper.png" />
              ),
              label: "Toiletries",
            },
            {
              icon: (
                <img src="https://img.icons8.com/ios/22/000000/ellipsis.png" />
              ),
              label: "Other",
            },
          ].map((cat) => (
            <Category icon={cat.icon} rounded label={cat.label} />
          ))}
        </Flex>
        <br />
      </>
    );
  };

  const Recommended = () => {
    const [id, setId] = useState(null);

    const [products, productsLoading, productsError] = useCollection(
      collection(db, "products"),
      {}
    );

    const addToCart = (product) => {
      setId(product.id);

      DB_Services.createOrder({
        product: {
          id: product.id,
          name: product.data().name,
          image: product.data().image,
          price: product.data().price,
        },
        user: authUser.uid,
      })
        .then(() => {
          message.success(
            `'${product.data().name}' successfully added to cart`
          );
          setId(null);
        })
        .catch((err) => {
          message.error("Failed");
          setId(null);
        });
    };

    return (
      <>
        <Title text="Recommended" />
        <br />
        {productsError && <p>Error....</p>}
        {productsLoading && basketLoading && (
          <Flex scrollY>
            {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((el) => (
              <SkeletonCustom />
            ))}
          </Flex>
        )}
        {products && !productsLoading && basket && (
          <Flex scrollY>
            {products.docs.map((product) => {
              return (
                <Product
                  image={product.data().image}
                  loading={id == product.id ? true : false}
                  add={() => {
                    if (authUser) {
                      addToCart(product);
                    } else {
                      router.push("/login");
                    }
                  }}
                  rounded
                  basket={
                    basket.docs.filter(
                      (order) => order.data().product.id == product.id
                    ).length > 0
                      ? true
                      : false
                  }
                  quantity={
                    0
                    // basket.docs.filter(
                    //   (order) => order.data().product.id == product.id
                    // )[0]
                  }
                  name={product.data().name}
                  price={product.data().price}
                  id={product.id}
                  remove={(id) => console.log(id)}
                  increment={(id) => console.log("increment", id)}
                  decrement={(id) => console.log("decrement", id)}
                />
              );
            })}
          </Flex>
        )}
      </>
    );
  };

  return (
    <Container>
      <Header />
      <Offers />
      <Categories />
      <Recommended />
    </Container>
  );
}

const Login = () => {
  return (
    <Page style={{ background: "#3F9B42" }}>
      <div style={{ alignItems: "center", width: "100%", marginTop: "20vh" }}>
        <span style={{ display: "block", margin: "0 auto", width: 150 }}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="150"
            style={{
              textAlign: "center",
            }}
            fill="rgb(229,224,85)"
            height="150"
            viewBox="0 0 24 24"
          >
            <path d="M19.029 13h2.971l-.266 1h-2.992l.287-1zm.863-3h2.812l.296-1h-2.821l-.287 1zm-.576 2h4.387l.297-1h-4.396l-.288 1zm2.684-9l-.743 2h-1.929l-3.474 12h-11.239l-4.615-11h14.812l-.564 2h-11.24l2.938 7h8.428l3.432-12h4.194zm-14.5 15c-.828 0-1.5.672-1.5 1.5 0 .829.672 1.5 1.5 1.5s1.5-.671 1.5-1.5c0-.828-.672-1.5-1.5-1.5zm5.9-7-.9 7c-.828 0-1.5.671-1.5 1.5s.672 1.5 1.5 1.5 1.5-.671 1.5-1.5c0-.828-.672-1.5-1.5-1.5z" />
          </svg>
        </span>

        <h3
          style={{
            color: "rgb(229,224,85)",
            textTransform: "uppercase",
            fontFamily: "Metropolis-Regular",
            letterSpacing: "7px",
            textAlign: "center",
          }}
        >
          Westgate shop
        </h3>
      </div>
      <button
        style={{
          position: "absolute",
          bottom: "5rem",
          width: "90%",
          border: "none",
          outline: "none",
          padding: 16,
          left: "5%",
          fontFamily: "Metropolis-Bold",
          color: "#707070",
          borderRadius: 12,
        }}
        onClick={signInFirebase}
      >
        <span style={{ marginRight: 12 }}>
          <svg
            width="24.43"
            height="25.00"
            viewBox="0 0 256 262"
            xmlns="http://www.w3.org/2000/svg"
            preserveAspectRatio="xMidYMid"
          >
            <path
              d="M255.878 133.451c0-10.734-.871-18.567-2.756-26.69H130.55v48.448h71.947c-1.45 12.04-9.283 30.172-26.69 42.356l-.244 1.622 38.755 30.023 2.685.268c24.659-22.774 38.875-56.282 38.875-96.027"
              fill="#4285F4"
            />
            <path
              d="M130.55 261.1c35.248 0 64.839-11.605 86.453-31.622l-41.196-31.913c-11.024 7.688-25.82 13.055-45.257 13.055-34.523 0-63.824-22.773-74.269-54.25l-1.531.13-40.298 31.187-.527 1.465C35.393 231.798 79.49 261.1 130.55 261.1"
              fill="#34A853"
            />
            <path
              d="M56.281 156.37c-2.756-8.123-4.351-16.827-4.351-25.82 0-8.994 1.595-17.697 4.206-25.82l-.073-1.73L15.26 71.312l-1.335.635C5.077 89.644 0 109.517 0 130.55s5.077 40.905 13.925 58.602l42.356-32.782"
              fill="#FBBC05"
            />
            <path
              d="M130.55 50.479c24.514 0 41.05 10.589 50.479 19.438l36.844-35.974C195.245 12.91 165.798 0 130.55 0 79.49 0 35.393 29.301 13.925 71.947l42.211 32.783c10.59-31.477 39.891-54.251 74.414-54.251"
              fill="#EB4335"
            />
          </svg>
        </span>
        Sign in with Google
      </button>
      <div style={{ position: "absolute", bottom: "1.5rem", width: "100%" }}>
        <p
          style={{
            color: "#E5E055",
            width: "100%",
            textAlign: "center",
            fontSize: "0.9rem",
          }}
        >
          Not a member?{" "}
          <button
            style={{
              border: "none",
              outline: "none",
              background: "transparent",
              textDecoration: "underline",
              color: "#E5E055",
              fontSize: "0.9rem",
            }}
          >
            Sign up
          </button>
        </p>
      </div>
    </Page>
  );
};
