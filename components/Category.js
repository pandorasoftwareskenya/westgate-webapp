import { useRouter } from "next/router";

export default function Category({ icon, label, rounded }) {
  const router = useRouter();

  return (
    <div style={{ width: 56, marginRight: 8 }}>
      <button
        onClick={() => router.push(`/${label.toLowerCase()}`)}
        style={{
          width: "56px",
          height: "56px",
          background: "#f1f1f1",
          borderRadius: rounded ? "6px" : "0px",
          justifyContent: "center",
          alignItems: "center",
          border: "none",
          outline: "none",
        }}
      >
        {icon}
      </button>
      <p
        style={{
          color: "#707070",
          width: "100%",
          fontFamily: "Metropolis-Regular",
          textAlign: "center",
          fontSize: "0.7rem",
          margin: 0,
          marginTop: 4,
        }}
      >
        {label}
      </p>
    </div>
  );
}
