export default function Search({
  placeholder,
  onChange,
  value,
  style,
  rounded,
}) {
  return (
    <input
      value={value}
      style={{
        ...style,
        padding: "16px",
        borderRadius: rounded ? "6px" : "0px",
        outline: "none",
        border: "none",
        background: "#f1f1f1",
      }}
      placeholder={placeholder}
      onChange={onChange}
    />
  );
}
